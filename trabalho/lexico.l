%option yylineno
%option yywrap

%{
#include <stdio.h>
#include "parser.h"
#include "y.tab.h"

FILE *yyin, *yyout;
%}

DIGITO[0-9]+
ID[a-zA-Z][_a-zA-Z0-9]*
WHITESPACE[ ]
COMMENT "//".*

%%
let										{return LET;}
def										{return DEF;}
if										{return IF;}
else									{return ELSE;}
while									{return WHILE;}
break									{return BREAK;}
return								{return RETURN;}
continue							{return CONTINUE;}
{ID}        					{strcpy(yylval.id, yytext); return IDENT;}
{DIGITO}							{yylval.num = atoi(yytext); return DIGIT;}
{COMMENT}							{}
"||"									{return OR;}
"("										{return OPENPAR;}
")"										{return CLOSEPAR;}
"{"										{return OPENBRACE;}
"}"										{return CLOSEBRACE;}
"-"										{return MINUS;}
"="										{return ASSIGN;}
","										{return COMMA;}
";"										{return SEMICOLON;}
"+"										{return PLUS;}
"*"										{return TIMES;}
"/"										{return DIV;}
"<"										{return LOW;}
">"										{return GREAT;}
"<="									{return LE;}
">="									{return GE;}
"=="									{return EQUAL;}
"!="									{return DIF;}
"&&"									{return AND;}
"!"										{return NOT;}
\n
[ \t]+					      ;
.                     yyerror("ERROR");

%%

int yywrap(){
	return 1;
}
