%{
#include<stdio.h>
#include<stdbool.h>
#include<stdlib.h>
#include <string.h>
#include <stdarg.h>
#include "parser.h"

#define YYDEBUG 1
extern FILE *yyout;

int yylex(void);

astNode *constante(int valor);
astNode *id(char value[20]);
astNode *criaTreeNode(int no_token, int nfilhos, ...);
int principal = 0;
int cont = 0;
char funcPrint[20];
char funcMain[20];
char param[20];
char var[20];

int yyerror(char *s);
%}

%union {
    int num;
    char id[20];
    astNode *ASTPonteiro;
};

%token AND EQUAL END ELSE GE GREAT IF LE OPENPAR LOW LOCAL NOT DIF OR ASSIGN PLUS MINUS
%token CLOSEPAR DIV TIMES WHILE PRINT STMT LET TYPE DEF COMMA COMPPARAM PARAM OPENBRACE
%token CLOSEBRACE BLOCK DCVAR ASSIGNSTMT COMPASSIGN RETURN EXPR RETURNSTMT STMTSTMT NEXPRFUNCCALL
%token VOLTASTMT STMTBREAK STMTIF STMTELSE STMTCONTINUE STMTFUNCCALL FUNCCALL FUNCNARGLIST FUNCNNARGLIST FUNCARGLIST

%token <num> DIGIT
%token <id> IDENT CONTINUE BREAK SEMICOLON

%type <ASTPonteiro> inicio expr
%type <ASTPonteiro> program decvar compdecvar decfunc paramlist compparamlist block ndecvar stmt assign nexpr nstmt nelse
%type <ASTPonteiro> funccall narglist nnarglist arglist

%nonassoc IF
%nonassoc ELSE

%left OR
%left AND
%left EQUAL DIF
%left LOW GREAT LE GE
%left PLUS MINUS
%left TIMES DIV
%right NOT
%left UMINUS
%left OPENPAR CLOSEPAR

%start inicio

%%

inicio
  : program  {verificaErros(); fprintf(yyout, "[program "); criaTree($1); fprintf(yyout, "]\n"); }
  |  {$$ = NULL;}
  ;

program
  : decfunc program       {$$ = criaTreeNode(STMT, 2, $1, $2); }
  | decvar program        {$$ = criaTreeNode(STMT, 2, $1, $2); }
  |                       {$$ = NULL;}
	;

decvar
  : LET IDENT compdecvar SEMICOLON {strcpy(var, $2); $$ = criaTreeNode(TYPE, 2, id($2),$3); }
	;

decfunc
  : DEF IDENT OPENPAR paramlist CLOSEPAR block  {strcpy(var, $2); if(strcmp($2, "main")==0) principal++; strcpy(funcPrint, $2); strcpy(funcMain, $2); verificaErros(); $$ = criaTreeNode(DEF, 3, id($2), $4, $6); }
	;

compdecvar
  : {$$ = NULL; }
	| ASSIGN expr {$$ = criaTreeNode(SEMICOLON, 1, $2); }
	;

paramlist
  :                          {$$ = NULL; }
  |	IDENT compparamlist      {strcpy(param,$1); $$ = criaTreeNode(PARAM, 2, id($1),$2); }
  ;

compparamlist
  :                                   {$$ = NULL ;}
  | COMMA IDENT compparamlist     {$$ = criaTreeNode(COMPPARAM, 2, id($2),$3); }
  ;

block
  : OPENBRACE ndecvar nstmt CLOSEBRACE {$$ = criaTreeNode(BLOCK, 3, $2, $3); }
  ;

ndecvar
  :                {$$ = NULL ;}
  | decvar ndecvar {$$ = criaTreeNode(DCVAR, 2, $1, $2); }
  ;

nstmt
  :            {$$ = NULL ;}
	| stmt nstmt {$$ = criaTreeNode(VOLTASTMT, 2, $1, $2); }
	;

stmt
      : assign SEMICOLON                      {$$ = criaTreeNode(ASSIGNSTMT, 1, $1); }
      | RETURN nexpr SEMICOLON                {$$ = criaTreeNode(RETURNSTMT, 1, $2); }
      | BREAK SEMICOLON                       {$$ = criaTreeNode(STMTBREAK, 2, $1, $2); }
      | CONTINUE SEMICOLON                    {$$ = criaTreeNode(STMTCONTINUE,2,$1, $2); }
      | IF OPENPAR expr CLOSEPAR block nelse  {$$ = criaTreeNode(STMTIF, 3, $3,$5,$6); }
      | WHILE OPENPAR expr CLOSEPAR block     {$$ = criaTreeNode(WHILE, 2, $3,$5); }
      | funccall SEMICOLON                    {$$ = criaTreeNode(STMTFUNCCALL, 1, $1); }
      ;

nelse
      :                                       {$$ = NULL;}
	    | ELSE block                            {$$ = criaTreeNode(STMTELSE, 1, $2); }
	    ;

assign
      : IDENT ASSIGN expr                     {$$ = criaTreeNode(COMPASSIGN, 2, id($1), $3); }
 	    ;

nexpr
      : {$$ = NULL ;}
      | expr                                  {$$ = criaTreeNode(EXPR, 1, $1); }
      ;

funccall
      : IDENT OPENPAR narglist CLOSEPAR       {$$ = criaTreeNode(FUNCCALL, 2, id($1), $3); }
      ;

narglist
      : {$$ = NULL ;}
      | arglist                               {$$ = criaTreeNode(FUNCNARGLIST, 1, $1); }
      ;

arglist
      : expr nnarglist                        {$$ = criaTreeNode(FUNCARGLIST, 2, $1, $2); }
      ;

nnarglist
      : {$$ = NULL ;}
      | COMMA expr nnarglist                  {$$ = criaTreeNode(FUNCNNARGLIST, 2, $2, $3); }
      ;

expr
      : DIGIT                                  { $$ = constante($1);}
      | IDENT                                  { $$ = id($1);}
      | NOT expr                               { $$ = criaTreeNode(NOT, 1, $2);}
      | MINUS expr %prec UMINUS                { $$ = criaTreeNode(UMINUS, 1, $2);}
      | expr PLUS expr                         { $$ = criaTreeNode(PLUS, 2, $1, $3); }
      | expr MINUS expr                        { $$ = criaTreeNode(MINUS, 2, $1, $3); }
      | expr TIMES expr                        { $$ = criaTreeNode(TIMES, 2, $1, $3); }
      | expr DIV expr                          { $$ = criaTreeNode(DIV, 2, $1, $3); }
      | expr OR expr                           { $$ = criaTreeNode(OR, 2, $1, $3); }
      | expr AND expr                          { $$ = criaTreeNode(AND, 2, $1, $3); }
      | expr LOW expr                          { $$ = criaTreeNode(LOW, 2, $1, $3); }
      | expr LE expr                           { $$ = criaTreeNode(LE, 2, $1, $3); }
      | expr GREAT expr                        { $$ = criaTreeNode(GREAT, 2, $1, $3); }
      | expr GE expr                           { $$ = criaTreeNode(GE, 2, $1, $3); }
      | expr EQUAL expr                        { $$ = criaTreeNode(EQUAL, 2, $1, $3); }
      | expr DIF expr                         { $$ = criaTreeNode(DIF, 2, $1, $3); }
      | OPENPAR expr CLOSEPAR                  { $$ = criaTreeNode(OPENPAR, 1, $2); }
      | funccall nexpr                         { $$ = criaTreeNode(NEXPRFUNCCALL, 2, $1, $2); }
;


%%

extern char *yytext;

//Folhas--------
astNode *constante(int valor) {
    astNode *an = malloc(sizeof(astNode));

    an->tipoNodo = Const;
    an->constante.valorConst = valor;

    return an;
}
astNode *id(char value[20]) {
    astNode *an = malloc(sizeof(astNode));

     an->tipoNodo = Identif;
     strcpy(an->ident.valor, value);

    return an;
}
//---------------

//Criacao de no da arvore
astNode *criaTreeNode(int no_token, int nfilhos, ...) {
    va_list ap; //lista de n argumentos da funcao

    astNode *an = malloc(sizeof(astNode));
    int i;

	an->astn.filhos = malloc(nfilhos * sizeof(astNode *));
	va_start(ap, nfilhos); // a lista de n argumentos comeca a partir de nfilhos

    an->tipoNodo = Ast;
    an->astn.token = no_token;
    an->astn.n_filhos = nfilhos;

    i = 0;
    while (i < nfilhos) {
		an->astn.filhos[i] = va_arg(ap, astNode*);
		i++;
    }

    va_end(ap);
    return an;
}
//------
verificaErros(){
	if (principal > 1) yyerror("erroMain");
  	if (strcmp(funcPrint, "print")==0) yyerror("erroPrint");
}

int valida(){
	if (strcmp(funcMain, "main")==0) cont++;
	if (cont > 1)	yyerror("erroMain");
	if (cont == 0){
		int a;
		a = 1;
		return a;
	}

	return 0;
}

int yyerror(char *s) {
    	printf("Erro");
	exit(1);
}


int main(int argc, char *argv[])
{
	int b;
	char *eu;
	if (argc != 3){
		printf("Erro. Entrada fora do padrao.");
		return 1;
	}
	FILE *input = fopen(argv[1], "r");
	FILE *output = fopen(argv[2], "w");

	extern FILE *yyin;
	extern FILE *yyout;
	yyout = output;
	yyin = input;

	yyparse();
	b = valida();
	if (b == 1){
		rewind(yyout);
		while(feof(yyout) == 0){
			output = fopen(argv[2], "w");
			yyerror("bla");
		}
	}

    return 0;
}
